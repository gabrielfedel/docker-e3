FROM centos:7.6.1810

RUN yum update -y \
  && yum groups mark convert \
  && yum groups install -y "Development Tools" \
  && yum install -y \
      blosc-devel   \ 
      boost-devel   \ 
      darcs         \ 
      dkms          \ 
      hdf5-devel    \
      lesstif-devel \ 
      mercurial     \ 
      procServ      \ 
      python-devel   \
         ipmitool               \
     kernel-devel           \
     libXmu-devel           \
     libXp-devel            \
     libXpm-devel           \
     libXt-devel            \
     libcurl-devel          \
     liberation-fonts-common\
     liberation-mono-fonts  \
     liberation-narrow-fonts\
     liberation-sans-fonts  \
     liberation-serif-fonts \
     libjpeg-turbo-devel    \
     libpng12-devel         \
     libraw1394             \
     libtiff-devel          \
     libtirpc-devel         \
     libusb-devel           \
     libusbx-devel          \
     libxml2-devel          \
     libzip-devel           \
     logrotate              \
     net-snmp               \
     net-snmp-devel         \
     net-snmp-utils         \
     opencv-devel           \
     pcre-devel             \
     perl-CPAN              \
     readline-devel      \
     symlinks            \
     systemd-devel       \
     tclx                \
     xorg-x11-fonts-misc \
     wget\
     sudo\
     python3\
     python3-pip\
     tree \
     graphviz\
  && yum clean all \
  && rm -rf /var/cache/yum

# install re2c
RUN wget https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/e/epel-release-7-12.noarch.rpm
RUN rpm -Uvh epel-release*rpm
RUN yum -y install re2c

# install base, require and basic modules
RUN cd /root && git clone https://github.com/icshwi/e3
WORKDIR /root/e3
RUN /usr/bin/yes | /bin/bash e3_building_config.bash -t /epics -b 3.15.6 setup
RUN /bin/bash e3.bash base
RUN /bin/bash e3.bash req
RUN /bin/bash e3.bash -c mod

# install pyepics
RUN pip3 install pyepics

# load e3 env variables on .bashrc
RUN echo "export EPICS_SRC=/root/e3" >> /root/.bashrc
RUN echo "source /root/e3/tools/setenv" >> /root/.bashrc

CMD "/bin/bash"
